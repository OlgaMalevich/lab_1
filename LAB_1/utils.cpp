#include "utils.h"

hr_clock::time_point Timer::start_time = hr_clock::now();

void Timer::start() {
	start_time = hr_clock::now();
}

double Timer::end() {
	auto end_time = hr_clock::now();
	return chrono::duration_cast<ms>(end_time - start_time).count() / 1000.;
}

void Timer::print() {
	cout << "elapsed: " << setprecision(3) << end() << " s" << endl;
}

// generate random number in [-100, 100]

int get_random_number() {
	//return (rand() % 200) - 100;
	return (rand() % 10);
}

matrix_t get_square_matrix(ulong matrix_size) {
	matrix_t matrix(matrix_size*matrix_size);
	for (int i = 0; i < matrix_size*matrix_size; i++) {
		matrix[i] = get_random_number();
	}
	return matrix;
}

ostream& print_square_matrix(matrix_t& matrix) {
	ulong matrix_size = sqrt(matrix.size());
	for (int i = 0; i < matrix_size; i++) {
		for (int j = 0; j < matrix_size; j++) {
			cout << matrix[i*matrix_size + j] << " ";
		}
		cout << endl;
	}

	return cout;
}