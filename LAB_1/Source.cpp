#include "lab1.h"
#include <cstdlib> // ��� system
#include <sstream>
const string USAGE = "./lab1 -n <rows> -m <cols> -r <block_size> [-o <out_file>='output.txt']";

int main(int argc, char* argv[]) {
	system("pause");
	if (argc < 7) {
		cerr << "Error. Usage is:" << endl;
		system("pause");
		return 1;
	}
	cout << "before params parse" << endl;
	system("pause");

	int matrix_size, thread_count, block_size;
	string filename = "output.txt";

	for (int i = 1; i < argc; i += 2) {
		if (string(argv[i]) == "-n") {
			matrix_size = atoi(argv[i + 1]);
		}
		else if (string(argv[i]) == "-t") {
			thread_count = atoi(argv[i + 1]);
		}
		else if (string(argv[i]) == "-r") {
			block_size = atoi(argv[i + 1]);
		}
		else {
			cout << "Invalid parameters" << endl;
			return 1;
		}
	}
	cout << "After param parse" << endl;
	fstream fout;
	fout.open(filename, fstream::out | fstream::app);
	if (matrix_size%block_size != 0) {
		cout << "Invalid parameters matrix size or block size";
		system("pause");
		fout.close();
		return 1;
	}

	omp_set_dynamic(0);
	omp_set_num_threads(8);

	matrix_t m1 = get_square_matrix(matrix_size);
	matrix_t m2 = get_square_matrix(matrix_size);
	matrix_t m3(matrix_size*matrix_size, 0);

	fout << "block_size: " << block_size << "\n" << "matrix_size: " << matrix_size << "x" << matrix_size << "\n" << "thread_count: " << thread_count << endl;
	Timer::start();
	block_multiply(m1, m2, m3, block_size, thread_count);
	fout << Timer::end() << " ";
	fout << endl;
	system("pause");
	fout.close();
	return 0;
}

void block_multiply(matrix_t& m1, matrix_t& m2, matrix_t& result, int block_size, int thread_count) {
	//cout << "\nm1 = " << endl;
	//for (vector<int>::iterator it = m1.begin(); it != m1.end(); ++it) {
	//	cout << *it << " ";
	//}
	//cout << "\nm2 = " << endl;
	//for (vector<int>::iterator it = m2.begin(); it != m2.end(); ++it) {
	//	cout << *it << " ";
	//}
	ulong matrix_size = sqrt(m1.size());
	int block_count = matrix_size / block_size;

#pragma omp parallel num_threads(thread_count)
	{
		//int thread_num = omp_get_thread_num();
		//std::stringstream ss;
		//ss << "outputFile" << thread_num << ".txt";
		//string filename = ss.str();
		//fstream fout;
		//fout.open(filename, fstream::out | fstream::app);
		//
#pragma omp for
		for (int n = 0; n < block_count*block_count; n++)
		{
			//����������� n-�� ���� �������������� �������
			//fout << " now n = " << n << endl;
			int block_row = n / block_count;
			int block_column = n % block_count;
			//fout << "block_row = " << block_row << endl;
			//fout << "block_column = " << block_column << endl;
			//����� ����������� ������ block_row �� ������ ������� �� ������� block_column �� ������ �������
			//� �������� ��� � N-�� ���� �������������� �������
			int result_first_item = (n / block_count)*matrix_size*block_size + (n % block_count)*block_size;
			for (int block_number = 0; block_number < block_count; block_number++)
			{
				int matrix1_first_item = block_row*block_size*matrix_size + block_number*block_size;
				int matrix2_first_item = block_column*block_size + block_number*block_size*matrix_size;
				//fout << "matrix1_first_item = " << matrix1_first_item << endl;
				//fout << "matrix2_first_item = " << matrix2_first_item << endl;
				//fout << "result_first_item = " << result_first_item << endl;
				for (int i = 0; i < block_size; i++)
				{
					for (int j = 0; j < block_size; j++)
					{
						int result_item = 0;
						//����������� i-j ������
						for (int k = 0; k < block_size; k++)
						{
							result_item += m1[matrix1_first_item + i*matrix_size + k] * m2[matrix2_first_item + k*matrix_size + j];
						}
						//fout << n << ") result[" << result_first_item << "] = " << result_item << endl;
						result[result_first_item + matrix_size*i + j] += result_item;
					}
				}
			}
		}
		//fout.close();
	}
	//cout << "\nresult = " << endl;
	//for (vector<int>::iterator it = result.begin(); it != result.end(); ++it) {
	//	cout << *it << " ";
	//}
	return;
}
